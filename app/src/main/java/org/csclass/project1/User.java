package org.csclass.project1;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Copyright 2016 TEALS, Inc. All rights reserved.
 * Created by ron on 9/1/16.
 */
public class User {
    public static final String USER_NAME = "UserName";
    public static final int NO_USER = -1;
    public static final int NO_AVATAR = 0;

    private Context context;
    private String name;
    private int imageId;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public void save() {
        SharedPreferences prefs = context.getSharedPreferences("", Activity.MODE_PRIVATE);
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(name, imageId);
            editor.apply();
        }
    }

    public static User createUser(Context context, String name, int imageId) {
        User user = new User();
        user.setContext(context);
        user.setName(name);
        user.setImageId(imageId);
        user.save();
        return user;
    }

    public static User getUser(Context context, String name) {
        SharedPreferences prefs = context.getSharedPreferences("", Activity.MODE_PRIVATE);
        int imageId = prefs.getInt(name, NO_USER);
        if (imageId == NO_USER) {
            return null;
        }

        User user = new User();
        user.setContext(context);
        user.setName(name);
        user.setImageId(imageId);
        return user;
    }
}
