package org.csclass.project1;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Intent intent = getIntent();
        String name = intent.getStringExtra(User.USER_NAME);
        User user = User.getUser(getApplicationContext(), name);
        if (user != null) {
            TextView username = (TextView) findViewById(R.id.username);
            if (username != null) {
                username.setText(user.getName());
            }
            ImageView avatar = (ImageView) findViewById(R.id.avatar);
            if (avatar != null) {
                avatar.setImageDrawable(getDrawableByName("avatar"+user.getImageId()));
            }
        }
    }

    private Drawable getDrawableByName(String name) {
        Resources resources = getResources();
        final int resourceId = resources.getIdentifier(name, "drawable", getPackageName());
        return resources.getDrawable(resourceId);
    }
}
