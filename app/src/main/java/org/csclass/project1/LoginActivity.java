package org.csclass.project1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button submit = (Button) findViewById(R.id.submit);
        if (submit != null) {
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    processUser();
                }
            });
        }
    }

    private void processUser() {
        EditText edit = (EditText)findViewById(R.id.username);
        if (edit != null) {
            String name = edit.getText().toString();
            User user = User.getUser(getApplicationContext(), name);
            if (user == null) {
                User.createUser(getApplicationContext(), name, User.NO_AVATAR);
                Intent intent = new Intent(this, GalleryActivity.class);
                intent.putExtra(User.USER_NAME, name);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(this, WelcomeActivity.class);
                intent.putExtra(User.USER_NAME, user.getName());
                startActivity(intent);
                finish();
            }
        }
    }
}
