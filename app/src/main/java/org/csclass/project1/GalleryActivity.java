package org.csclass.project1;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity {
    private List<ImageView> avatars = new ArrayList<>();
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Intent intent = getIntent();
        String name = intent.getStringExtra(User.USER_NAME);
        user = User.getUser(getApplicationContext(), name);

        avatars.add((ImageView) findViewById(R.id.avatar1));
        avatars.add((ImageView) findViewById(R.id.avatar2));
        avatars.add((ImageView) findViewById(R.id.avatar3));
        avatars.add((ImageView) findViewById(R.id.avatar4));
        avatars.add((ImageView) findViewById(R.id.avatar5));
        avatars.add((ImageView) findViewById(R.id.avatar6));

        AvatarClickListener listener = new AvatarClickListener();
        for (ImageView avatar : avatars) {
            avatar.setOnClickListener(listener);
        }
    }

    private class AvatarClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int imageId = 0;
            switch (v.getId()) {
                case R.id.avatar1:
                    imageId = 1;
                    break;
                case R.id.avatar2:
                    imageId = 2;
                    break;
                case R.id.avatar3:
                    imageId = 3;
                    break;
                case R.id.avatar4:
                    imageId = 4;
                    break;
                case R.id.avatar5:
                    imageId = 5;
                    break;
                case R.id.avatar6:
                    imageId = 6;
                    break;
            }
            user.setImageId(imageId);
            user.save();
            Intent intent = new Intent(GalleryActivity.this, WelcomeActivity.class);
            intent.putExtra(User.USER_NAME, user.getName());
            startActivity(intent);
            finish();
        }
    }
}
